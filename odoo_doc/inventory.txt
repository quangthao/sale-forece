Invenroty

* Quản lý kho

+ Sản phẩm
  . Đơn vị đo lường cho các sản phẩm bán, mua
  . Có thể chỉ định đơn vị đo lường trong kho của mình và đơn vị đo lường khi mua sp
  . Bổ sung: 
    > Quy tắc hàng tồn tối thiểu để đảm bảo bạn luôn có số lượng sp tối thiểu trong kho, có thể đưa cảnh báo...
    > Đặt hàng: hệ thống sẽ giữ trước số lượng tương ứng trên đơn hàng

+ Kho
  Sự khác biệt giữa kho và địa điểm là kho là 1 tòa nhà hay địa điểm thực tế chứa hàng, bạn có thể thiết lập nhiều kho và tạo sự chuyển dời sp 
  giữa các kho; còn địa điểm là 1 không gian trong kho như phòng, lối, gian hàng, kệ... Có 3 loại địa điểm là địa điểm vật lý là 1 phần của kho,
  địa điểm đối tác là 1 không gian trong kho của nhà cung cấp, địa điểm ảo là nơi có thể đặt sp khi k thể đặt trong kho nữa. Chúng rất hữu ích
  khi bạn muốn đưa sp bị thất lạc ra khỏi kho của mình hay tính đến các sp đang mua đến kho của mình.

+ Điều chỉnh hàng tồn kho: tiếp tế từ kho khác

+ Giao hàng
  . Luồng giao hàng: 3 cách vận chuyển từ kho
    > 1 bước (vận chuyển) gửi trực tiếp từ kho
    > 2 bước (nhận hàng + vận chuyển): đưa hàng đến địa điểm đầu ra trước khi vận chuyển
    > 3 bước (nhận hàng + đóng gói + vận chuyển): đặt các gói hàng vào vị trí chuyên dụng sau đó mang chúng đến vị trí đầu ra để vận chuyển	
  . Luồng mua hàng: 3 cách
    > 1 bước: nhận hàng trực tiếp tại kho
    > 2 bước: dỡ hàng vị trí đầu vào sau đó nhập vào kho
    > 3 bước: dỡ hàng vị trí đầu vào qua ktra chất lượng sau đó nhập kho

+ Ký gửi

+ Lập kế hoạch: hoạch định ngày cam kết giao hàng để KH dự đoán được ngày nhận sp của mình

+ Số lô và số seri: số lô quản lý lô hàng đc sx hàng loạt còn số seri được dùng quản lý từng sản phẩm. Ngoài ra còn có ngày hết hạn của từng lô hay mỗi sp.

		
* Vận chuyển

+ Thiết lập phương thức vận chuyển 
  . Thiết lập chung: có giá cố định hay miễn phí nếu đơn hàng lớn hơn 1 giá trị nào đó, có thể cấu hình giá theo quy tắc, có thể giới hạn phương thức
    giao hàng này cho 1 vài địa điểm.
  . Tích hợp người gửi hàng bên thứ 3

+ Hoạt động vận chuyển
  . Đề xuất chi phí vận chuyển: có 2 cách là thỏa thuận với kh và cho nó vào đơn hàng và lập hóa đơn chi phí vận chuyển thực tế
  . Quản lý nhiều gói hàng trong một đơn hàng
  . IN nhãn vận chuyển

* Advanced routes

* Mã vạch