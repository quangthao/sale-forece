import odoo
import json
import logging
_logger = logging.getLogger(__name__)

class MyPetAPI(odoo.http.Controller):
    @odoo.http.route('/foo', auth='public')
    def foo_handler(self):
        return " 'foo' API!"

    @odoo.http.route('/bar', auth='public')
    def bar_handler(self):
        return json.dumps({
            "content": "Welcome to 'bar' API!"
        })

    @odoo.http.route(['/pet/<dbname>/<id>'], type='http', auth="none", sitemap=False, cors='*', csrf=False)
    def pet_handler(self, dbname, id, **kw):
        model_name = "my.pet"
        try:
            registry = odoo.modules.registry.Registry(dbname)
            with odoo.api.Environment.manage(), registry.cursor() as cr:
                env = odoo.api.Environment(cr, odoo.SUPERUSER_ID, {})
                rec = env[model_name].search([('id', '=', int(id))], limit=1)
                response = {
                    "status": "ok",
                    "content": {
                        "name": rec.name,
                        "nickname": rec.nickname,
                        "description": rec.description,
                        "age": rec.age,
                        "weight": rec.weight,
                        "dob": rec.dob.strftime('%d/%m/%Y'),
                        "gender": rec.gender,
                    }
                }
        except Exception:
            response = {
                "status": "error",
                "content": "not found"
            }
        return json.dumps(response)

    @odoo.http.route(['/create/<dbname>/<id>'], type='http', auth="none", sitemap=False, cors='*', csrf=False)
    def create_handler(self, dbname, id, **kw):
        model_name = "my.pet"
        try:
            registry = odoo.modules.registry.Registry(dbname)
            with odoo.api.Environment.manage(), registry.cursor() as cr:
                env = odoo.api.Environment(cr, odoo.SUPERUSER_ID, {})
                rec = env[model_name].search([('id', '=', int(id))], limit=1)
                if not rec:
                    print(rec)
                    print('createeeeeeeeeeeeeeeeeeee')
                    rec.create({
                        'name': 'Ant',
                        'nickname': 'Naruto',
                        'age': int(id),
                        'gender': 'male'
                    })

                    response = {
                        "status": "ok",
                        "content": "create"
                    }
                    return json.dumps(response)
                else:
                    print(rec)
                    return "ID already exists"
        except Exception:
            return "Create make an error"

    @odoo.http.route(['/write/<dbname>/<id>'], type='http', auth="none", sitemap=False, cors='*', csrf=False)
    def write_handler(self, dbname, id, **kw):
        model_name = "my.pet"
        try:
            registry = odoo.modules.registry.Registry(dbname)
            with odoo.api.Environment.manage(), registry.cursor() as cr:
                env = odoo.api.Environment(cr, odoo.SUPERUSER_ID, {})
                rec = env[model_name].search([('id', '=', int(id))], limit=1)
                if rec:
                    rec.write({
                        'nickname': ' Write Naruto',
                        'age': rec.age + 34,
                        'gender': 'male'
                    })

                    response = {
                        "status": "ok",
                        "content": "write"
                    }
                    return json.dumps(response)

        except Exception:
            return "Write an error!"