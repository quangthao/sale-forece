# -*- coding: utf-8 -*-
from odoo import api, fields, models

class SaleOrderLineInherit(models.Model):
    _inherit="sale.order.line"

    product_discount_estimated = fields.Float(related='product_template_id.discount_percent')
    discount = fields.Float(string='Discount (%)', digits='Discount', related='product_discount_estimated',)
    warranty_discount = fields.Float(string='Warranty Discount (%)', digits='Discount',
                            related='product_discount_estimated', store=True)

    @api.depends('discount')
    def _compute_price_subtotal_depends_discount(self):
        for rec in self:
            rec.price_subtotal = rec.price_subtotal * (100 - rec.discount) / 100.0

