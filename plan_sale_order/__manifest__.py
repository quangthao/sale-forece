{
    'name': "Plan Sale Order",
    'summary': """Plan Sale Order""",
    'description': """Plan Sale Order""",
    'author': "Quang",
    'website': "",
    'category': 'WeUp',
    'version': '1.2',
    'sequence': 2,
    'depends': [
        'base',
        'crm',
        'mail',
        'contacts',
        'sale_management',
    ],
    'data': [
        'security/ir.model.access.csv',
        # 'security/security.xml',
        'views/plan_sale_order.xml',
        # 'views/list_of_approvers.xml',
        'views/sale_order_inherit.xml',

    ],
    'qweb': [],
    'installable': True,
    'application': True,
}