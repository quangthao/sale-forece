# -*- coding: utf-8 -*-
from odoo import api, fields, models

class SaleOrderInherit(models.Model):
    _inherit="sale.order"

    sale_order_discount_estimated = fields.Integer(related='partner_id.discount_number')
    amount_discount = fields.Monetary(string='Discount Amount', store=True, compute='_amount_all',)
    customer_has_discount = fields.Boolean(related='partner_id.has_discount_code')

    @api.depends('order_line.price_total')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """
        for order in self:
            amount_untaxed = amount_tax = amount_discount = amount_total = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
                amount_discount = (amount_untaxed + amount_tax) * order.sale_order_discount_estimated/100
                amount_total = (amount_untaxed + amount_tax) - amount_discount
            order.update({
                'amount_untaxed': amount_untaxed,
                'amount_tax': amount_tax,
                'amount_discount': amount_discount,
                'amount_total': amount_total,
            })